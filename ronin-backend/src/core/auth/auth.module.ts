import { UsersModule } from './../../external/users/users.module';
import { CacheModule, forwardRef, Global, Module } from '@nestjs/common';

import { AuthService } from './auth.service';
import { AuthController } from './auth.controller';
import { DatabaseModule } from '../database/database.module';

@Global()
@Module({
    imports: [CacheModule.register(), DatabaseModule, UsersModule],
    controllers: [AuthController],
    providers: [AuthService],
    exports: [AuthService],
})
export class AuthModule {}
