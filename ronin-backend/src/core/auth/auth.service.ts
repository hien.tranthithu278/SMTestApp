import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { UsersService } from 'src/external/users/users.service';
import { LoginDto } from './auth.dto';

@Injectable()
export class AuthService {
    constructor(private readonly usersService: UsersService) {}

    async login(data: LoginDto) {
        const user = await this.usersService.findOne({
            where: {  password: data.password },
            rejectOnEmpty: false,
            nest: true,
            raw: true,
        });

        if (!user) throw new HttpException('Invalid user.', HttpStatus.UNAUTHORIZED);

        return {
            accessToken: Buffer.from(JSON.stringify({ userId: user.id }), 'utf-8').toString('base64'),
        };
    }
}
