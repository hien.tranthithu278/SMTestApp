import { ConfigModule, ConfigService } from '@nestjs/config';
import { Sequelize } from 'sequelize-typescript';
import { User } from 'src/external/users/user.entity';
import { Account } from '../../external/accounts/account.entity';
import { Asset } from '../../external/assets/asset.entity';
import { Currency } from '../../external/currencies/currency.entity';

export const databaseProviders = [
    {
        provide: 'SEQUELIZE',
        imports: [ConfigModule],
        useFactory: async (configService: ConfigService) => {
            const sequelize = new Sequelize({
                dialect: 'postgres',
                host: configService.get<string>('DB_HOST'),
                port: configService.get<number>('DB_PORT'),
                username: configService.get<string>('DB_USERNAME'),
                password: configService.get<string>('DB_PASSWORD'),
                database: configService.get<string>('DB_NAME'),
            });

            sequelize.addModels([User, Account, Asset, Currency]);

            await sequelize.sync({
                force: true,
            });

            return sequelize;
        },
        inject: [ConfigService],
    },
];
