import { FindOptions, NonNullFindOptions } from 'sequelize/dist';

const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
const numbers = '0123456789';

export function generateAddress(length: number) {
    let result = '';
    let charactersLength = numbers.length;

    for (let i = 0; i < length; i++) {
        result += numbers.charAt(Math.floor(Math.random() * charactersLength));
    }

    return result;
}

export function randomString(length: number) {
    let result = '';
    let charactersLength = characters.length;

    for (let i = 0; i < length; i++) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }

    return result;
}

export function buildOptions<T extends object>(conditions: FindOptions<T>) {
    const newConditions: NonNullFindOptions<T> = {
        rejectOnEmpty: false,
        nest: true,
        raw: true,
        ...conditions,
    };

    return newConditions;
}
