import { Inject, Injectable, Module } from '@nestjs/common';
import { CountOptions, FindOptions, NonNullFindOptions, UpdateOptions } from 'sequelize/dist';

import { AccountsService } from '../accounts/accounts.service';
import { User, userSchemaToken } from './user.entity';
import { CreateUserDto } from './user.dto';
import { randomString } from '../../helper/helper';

@Injectable()
export class UsersService {
    constructor(
        @Inject(userSchemaToken)
        private readonly userRepository: typeof User,
        private readonly accountsService: AccountsService,
    ) {}

    async count(conditions: CountOptions<User>) {
        return await this.userRepository.count(conditions);
    }

    async findOne(conditions: NonNullFindOptions<User>) {
        return await this.userRepository.findOne(conditions);
    }

    async create(data: CreateUserDto) {
        const newUser = await this.userRepository.create(data as any);

        await this.accountsService.create({ userId: newUser.id, name: randomString(5) });

        return newUser.toJSON();
    }

    async delete(conditions: FindOptions<User>) {
        return await this.userRepository.destroy(conditions);
    }
}
