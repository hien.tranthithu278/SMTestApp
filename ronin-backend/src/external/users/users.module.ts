import { Module, OnModuleInit } from '@nestjs/common';
import { DatabaseModule } from 'src/core/database/database.module';
import { AccountsModule } from '../accounts/accounts.module';
import { UsersController } from './users.controller';
import { UsersProviders } from './users.provider';
import { UsersService } from './users.service';

@Module({
    imports: [DatabaseModule, AccountsModule],
    controllers: [UsersController],
    providers: [UsersService, ...UsersProviders],
    exports: [UsersService, ...UsersProviders],
})
export class UsersModule implements OnModuleInit {
    constructor(private readonly usersService: UsersService) {}

    async onModuleInit() {
        await this.usersService.delete({ where: {} });

        const data = [
            {
                password: '123456',
            },
            {
                password: '654321',
            },
        ];

        if ((await this.usersService.count({})) !== data.length) {
            await this.usersService.delete({ where: {} });

            for (const d of data) {
                try {
                    await this.usersService.create(d);
                } catch (err) {
                    console.log(err);
                }
            }
        }
    }
}
