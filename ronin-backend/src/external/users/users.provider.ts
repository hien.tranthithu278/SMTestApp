import { User, userSchemaToken } from './user.entity';

export const UsersProviders = [
    {
        provide: userSchemaToken,
        useValue: User,
    },
];
