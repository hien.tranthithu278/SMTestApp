import { Table, Column, Model } from 'sequelize-typescript';

@Table({ tableName: 'users' })
export class User extends Model {
    @Column
    password: string;
}

export const userSchemaToken = 'USERS_REPOSITORY';
