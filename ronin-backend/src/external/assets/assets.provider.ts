import { assetSchemaToken, Asset } from './asset.entity';

export const AssetsProviders = [
    {
        provide: assetSchemaToken,
        useValue: Asset,
    },
];
