import { IsNumber, IsString } from 'class-validator';

export class TransferAssetDto {
    @IsString()
    symbol: string;

    @IsString()
    from: string;

    @IsString()
    to: string;

    @IsNumber()
    amount: number;
}

export class CreateAssetDto {
    @IsNumber()
    accountId: number;

    @IsNumber()
    currencyId: number;

    @IsNumber()
    balance: number;

    @IsString()
    symbol: string;
}
