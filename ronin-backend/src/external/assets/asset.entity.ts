import { Table, Column, Model, ForeignKey, BelongsTo } from 'sequelize-typescript';

import { Account } from '../accounts/account.entity';
import { Currency } from '../currencies/currency.entity';

@Table({ tableName: 'assets' })
export class Asset extends Model {
    @ForeignKey(() => Account)
    @Column
    accountId: number;

    @BelongsTo(() => Account, 'accountId')
    account: Account;

    @ForeignKey(() => Currency)
    @Column
    currencyId: number;

    @BelongsTo(() => Currency, 'currencyId')
    currency: Currency;

    @Column
    balance: number;

    @Column
    symbol: string;
}

export const assetSchemaToken = 'ASSETS_REPOSITORY';
