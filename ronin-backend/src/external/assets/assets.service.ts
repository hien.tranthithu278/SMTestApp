import { forwardRef, HttpException, HttpStatus, Inject, Injectable, Module } from '@nestjs/common';
import { FindOptions, NonNullFindOptions, UpdateOptions } from 'sequelize/dist';
import { AccountsService } from '../accounts/accounts.service';

import { assetSchemaToken, Asset } from './asset.entity';
import { CreateAssetDto, TransferAssetDto } from './assets.dto';

@Injectable()
export class AssetsService {
    constructor(
        @Inject(forwardRef(() => AccountsService))
        private readonly accountsService: AccountsService,
        @Inject(assetSchemaToken)
        private readonly assetRepository: typeof Asset,
    ) {}

    async find(conditions: FindOptions<Asset>) {
        return await this.assetRepository.findAll(conditions);
    }

    async create(data: CreateAssetDto) {
        return await this.assetRepository.create({ ...data });
    }

    async transfer(data: TransferAssetDto) {
        if (
            (await this.assetRepository.count({
                where: { symbol: data.symbol },
            })) < 2
        )
            throw new HttpException('Asset not found.', HttpStatus.NOT_FOUND);

        const sender = await this.accountsService.findOne({
            where: {
                address: data.from.trim(),
            },
            rejectOnEmpty: false,
            nest: true,
            raw: true,
        });

        const receiver = await this.accountsService.findOne({
            where: {
                address: data.to.trim(),
            },
            rejectOnEmpty: false,
            nest: true,
            raw: true,
        });

        if (!receiver || !sender) throw new HttpException('Address not found.', HttpStatus.NOT_FOUND);
        if (receiver.id === sender.id) return;

        const senderAsset = await this.assetRepository.findOne({
            where: {
                symbol: data.symbol,
                accountId: sender.id,
            },
            rejectOnEmpty: false,
            nest: true,
        });

        const receiverAsset = await this.assetRepository.findOne({
            where: {
                symbol: data.symbol,
                accountId: receiver.id,
            },
            rejectOnEmpty: false,
            nest: true,
        });

        if (!receiverAsset || !senderAsset) throw new HttpException('Asset not found.', HttpStatus.NOT_FOUND);

        if (data.amount > senderAsset.balance) throw new HttpException('Balance not enough.', HttpStatus.BAD_REQUEST);

        senderAsset.balance -= data.amount;
        receiverAsset.balance += data.amount;

        senderAsset.save();
        receiverAsset.save();
    }
}
