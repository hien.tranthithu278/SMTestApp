import { forwardRef, Module } from '@nestjs/common';
import { AccountsModule } from '../accounts/accounts.module';
import { AssetsProviders } from './assets.provider';
import { AssetsService } from './assets.service';
import { AssetsController } from './assets.controller';
import { DatabaseModule } from 'src/core/database/database.module';

@Module({
    imports: [DatabaseModule, forwardRef(() => AccountsModule)],
    controllers: [AssetsController],
    providers: [AssetsService, ...AssetsProviders],
    exports: [AssetsService, ...AssetsProviders],
})
export class AssetsModule {}
