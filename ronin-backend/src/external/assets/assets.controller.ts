import { AssetsService } from './assets.service';
import { Body, Controller, Get, Patch, Query } from '@nestjs/common';
import { FindOptions } from 'sequelize/dist';

import { Asset } from './asset.entity';
import { TransferAssetDto } from './assets.dto';
import { Currency } from '../currencies/currency.entity';

@Controller('assets')
export class AssetsController {
    constructor(private readonly assetsService: AssetsService) {}

    @Get('')
    async find(@Query('accountId') accountId: string) {
        return await this.assetsService.find({
            where: { accountId: +accountId },
            include: [{ model: Currency }],
            nest: true,
            raw: true,
        });
    }

    @Patch('transfer')
    async transfer(@Body() data: TransferAssetDto) {
        return await this.assetsService.transfer(data);
    }
}
