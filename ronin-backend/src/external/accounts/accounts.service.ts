import { CurrenciesService } from './../currencies/currencies.service';
import { Inject, Injectable, Module } from '@nestjs/common';
import { FindOptions, NonNullFindOptions, UpdateOptions } from 'sequelize/dist';
import { Op } from 'sequelize';

import { generateAddress } from 'src/helper/helper';
import { AssetsService } from '../assets/assets.service';
import { Account, accountSchemaToken } from './account.entity';
import { CreateAccountDto, UpdateAccountDto } from './accounts.dto';

@Injectable()
export class AccountsService {
    constructor(
        @Inject(accountSchemaToken)
        private readonly accountRepository: typeof Account,
        private readonly assetsService: AssetsService,
        private readonly currenciesService: CurrenciesService,
    ) {}

    async find(conditions: FindOptions<Account>) {
        return await this.accountRepository.findAll(conditions);
    }

    async findOne(conditions: NonNullFindOptions<Account>) {
        return await this.accountRepository.findOne(conditions);
    }

    async create(data: CreateAccountDto) {
        const account = await this.accountRepository.create(data as any);

        account.address = generateAddress(12);

        const currencies = await this.currenciesService.find({ where: { quote: { [Op.in]: ['EUR', 'JPY', 'USD'] } } });

        for (const currency of currencies) {
            await this.assetsService.create({
                accountId: account.id,
                currencyId: currency.id,
                balance: 0,
                symbol: currency.quote,
            });
        }

        await account.save();

        return account.toJSON();
    }

    async update(conditions: UpdateOptions<Account>, data: UpdateAccountDto) {
        return await this.accountRepository.update(data, conditions);
    }
}
