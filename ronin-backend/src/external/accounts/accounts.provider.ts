import { Account, accountSchemaToken } from './account.entity';

export const AccountsProviders = [
    {
        provide: accountSchemaToken,
        useValue: Account,
    },
];
