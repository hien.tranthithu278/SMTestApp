import { forwardRef, Module } from '@nestjs/common';
import { AccountsProviders } from './accounts.provider';
import { AccountsService } from './accounts.service';
import { AccountsController } from './accounts.controller';
import { AssetsService } from '../assets/assets.service';
import { DatabaseModule } from 'src/core/database/database.module';
import { AssetsModule } from '../assets/assets.module';
import { CurrenciesModule } from '../currencies/currencies.module';

@Module({
    imports: [DatabaseModule, forwardRef(() => AssetsModule), CurrenciesModule],
    controllers: [AccountsController],
    providers: [AccountsService, ...AccountsProviders],
    exports: [AccountsService, ...AccountsProviders],
})
export class AccountsModule {}
