import { IsNumber, IsOptional, IsString } from 'class-validator';

export class CreateAccountDto {
    @IsOptional()
    @IsNumber()
    userId?: number;

    @IsOptional()
    @IsString()
    name?: string;
}

export class UpdateAccountDto {
    @IsOptional()
    @IsString()
    name?: string;
}
