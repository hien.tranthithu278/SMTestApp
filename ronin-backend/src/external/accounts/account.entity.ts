import { Table, Column, Model, ForeignKey, BelongsTo } from 'sequelize-typescript';
import { User } from '../users/user.entity';

@Table({ tableName: 'accounts' })
export class Account extends Model {
    @Column
    name: string;

    @ForeignKey(() => User)
    @Column
    userId: number;

    @BelongsTo(() => User, 'userId')
    user: User;

    @Column
    address: string;
}

export const accountSchemaToken = 'ACCOUNTS_REPOSITORY';
