import { Body, Controller, Get, Inject, Param, Patch, Post, Query } from '@nestjs/common';
import { FindOptions, UpdateOptions } from 'sequelize/dist';

import { accountSchemaToken, Account } from './account.entity';
import { CreateAccountDto, UpdateAccountDto } from './accounts.dto';
import { generateAddress } from 'src/helper/helper';
import { AccountsService } from './accounts.service';

@Controller('accounts')
export class AccountsController {
    constructor(private readonly accountsService: AccountsService) {}

    @Get('')
    async find(@Query('userId') userId: string) {
        return await this.accountsService.find({ where: { userId: +userId }, nest: true, raw: true });
    }

    @Post('')
    async create(@Body() data: CreateAccountDto) {
        return await this.accountsService.create(data);
    }

    @Patch(':accountId')
    async update(@Param('accountId') accountId: number, @Body() data: UpdateAccountDto) {
        return await this.accountsService.update({ where: { id: accountId } }, data);
    }
}
