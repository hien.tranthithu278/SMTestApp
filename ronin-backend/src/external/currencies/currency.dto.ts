import { IsNumber, IsString } from 'class-validator';

export class CreateCurrencyDto {
    @IsNumber()
    rate: number;

    @IsString()
    base: string;

    @IsString()
    quote: string;
}
