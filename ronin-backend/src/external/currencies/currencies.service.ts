import { Inject, Injectable } from '@nestjs/common';
import { CountOptions, DestroyOptions, FindOptions } from 'sequelize/dist';
import { CreateCurrencyDto } from './currency.dto';
import { Currency, currencySchemaToken } from './currency.entity';

@Injectable()
export class CurrenciesService {
    constructor(@Inject(currencySchemaToken) private readonly currencyRepository: typeof Currency) {}

    async count(conditions: CountOptions<Currency>) {
        return await this.currencyRepository.count(conditions);
    }

    async find(conditions: FindOptions<Currency>) {
        return await this.currencyRepository.findAll(conditions);
    }

    async create(data: CreateCurrencyDto) {
        return await this.currencyRepository.create(data as any);
    }

    async delete(conditions: DestroyOptions<Currency>) {
        return await this.currencyRepository.destroy(conditions);
    }
}
