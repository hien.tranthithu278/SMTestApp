import { Table, Column, Model, DataType } from 'sequelize-typescript';

@Table({ tableName: 'currencies' })
export class Currency extends Model {
    @Column({ type: DataType.DOUBLE })
    rate: number;

    @Column
    base: string;

    @Column
    quote: string;
}

export const currencySchemaToken = 'CURRENCIES_REPOSITORY';
