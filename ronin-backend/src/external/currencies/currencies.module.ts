import { Module, OnModuleInit } from '@nestjs/common';
import fetch from 'node-fetch';
import { DatabaseModule } from 'src/core/database/database.module';

import { CurrenciesController } from './currencies.controller';
import { CurrenciesProviders } from './currencies.provider';
import { CurrenciesService } from './currencies.service';

@Module({
    imports: [DatabaseModule],
    controllers: [CurrenciesController],
    providers: [CurrenciesService, ...CurrenciesProviders],
    exports: [CurrenciesService, ...CurrenciesProviders],
})
export class CurrenciesModule implements OnModuleInit {
    constructor(private readonly currenciesService: CurrenciesService) {}

    async onModuleInit() {
        const rates = await (await fetch('https://api.stdio.vn/mini-apps/rates')).json();

        if ((await this.currenciesService.count({})) !== rates.length) {
            await this.currenciesService.delete({ where: {} });

            for (const rate of rates) {
                try {
                    await this.currenciesService.create({ rate: rate.rate, base: 'EUR', quote: rate.code });
                } catch (err) {
                    console.log(err);
                }
            }
        }
    }
}
