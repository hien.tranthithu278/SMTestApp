import { Controller, Get, Inject, Patch } from '@nestjs/common';
import { FindOptions, UpdateOptions } from 'sequelize/dist';
import { CurrenciesService } from './currencies.service';
import { currencySchemaToken, Currency } from './currency.entity';

@Controller('currencies')
export class CurrenciesController {
    constructor(private readonly currenciesService: CurrenciesService) {}

    @Get('')
    async find(conditions: FindOptions<Currency>) {
        return await this.currenciesService.find(conditions);
    }
}
