import { Currency, currencySchemaToken } from './currency.entity';

export const CurrenciesProviders = [
    {
        provide: currencySchemaToken,
        useValue: Currency,
    },
];
