import { ConfigModule } from '@nestjs/config';
import { Module } from '@nestjs/common';

import { DatabaseModule } from './core/database/database.module';
import { AccountsModule } from './external/accounts/accounts.module';
import { AssetsModule } from './external/assets/assets.module';
import { CurrenciesModule } from './external/currencies/currencies.module';
import { UsersModule } from './external/users/users.module';
import { AuthModule } from './core/auth/auth.module';

@Module({
    imports: [
        ConfigModule.forRoot({
            isGlobal: true,
        }),
        DatabaseModule,
        CurrenciesModule,
        AuthModule,
        AssetsModule,
        AccountsModule,
        UsersModule,
    ],
    controllers: [],
    providers: [],
})
export class AppModule {}
