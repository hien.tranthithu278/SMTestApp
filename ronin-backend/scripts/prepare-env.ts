/* eslint-disable @typescript-eslint/no-var-requires */
const fs = require('fs-extra');
const path = require('path');

const args = process.argv.slice(2);
const targetEnv = args[0];

const WORKING_DIR = path.join(__dirname, '..');
const targetEnvFile = path.join(WORKING_DIR, `.env.${targetEnv}`);
const generatedEnvFile = path.join(WORKING_DIR, '.env');

if (!fs.existsSync(targetEnvFile)) {
    console.log('# [ERROR] Invalid env file');
}

fs.copySync(targetEnvFile, generatedEnvFile);
fs.appendFileSync(
    generatedEnvFile,
    `

# !! THIS FILE IS GENERATED - DO NOT MODIFY MANUALY !!
# !! RUN 'yarn env' IF YOU WANT TO UPDATE THE ENV SET !!
# CURRENT ENVIRONMENT => ${targetEnv}
`,
);

console.log('# [DONE] Env file is created');
