export type ICurrency = "EUR" | "USD" | "JPY"

export interface IAsset {
    id: string;
    symbol: ICurrency;
    balance: number;
}

export interface IAccount {
    id: string,
    name: string,
    address: string
}

export interface IExchangeRate {
    id: string,
    rate: number,
    base: string,
    quote: string
}