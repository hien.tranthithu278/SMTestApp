import { useRef, useEffect } from "react";
import { Buffer } from "buffer";
import moment from "moment-timezone";
import { StatusCodes } from "http-status-codes";
import { get, isEmpty } from "lodash";

import { getStorageItem } from "./local-storage";

export const getToken = () => {
    return get(getStorageItem("auth"), "accessToken", "");
};

export const getAuthType = () => {
    return get(getStorageItem("auth"), "authType", "none");
};

// export const getImageURL = (src: string) => {
//     return `${IMAGE_URL}/${src}`;
// };

export const formatNumberToVND = (number?: number | undefined) => {
    if (!number) return "0₫";
    return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + "₫";
};

export const formatWithCommas = (number: number) => {
    if (number === null) return "";
    return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
};

export const formatTime = (timestamp: number, pattern = "HH:mm:ss") => {
    return moment.unix(timestamp).format(pattern);
};

export const formatDate = (timestamp: number, pattern = "DD/MM/YYYY") => {
    timestamp = !!timestamp ? timestamp : 0;
    return moment.unix(timestamp).format(pattern);
};

export const formatDateTime = (timestamp: number) => {
    return moment.unix(timestamp).format("DD/MM/YYYY HH:mm:ss");
};

export const extractResponseError = (err: any) => {
    const { response = null, message } = err;

    if (isEmpty(response)) {
        return message;
    }

    switch (response.status) {
        case StatusCodes.BAD_REQUEST:
        case StatusCodes.UNAUTHORIZED:
        case StatusCodes.NOT_FOUND:
            return response.data.message;
    }

    console.log("General response error");
    return message;
};

export const usePrevious = (value: any) => {
    const ref = useRef();
    useEffect(() => {
        ref.current = value;
    });
    return ref.current;
};

export const decodeToken = (token: string) => {
    return JSON.parse(Buffer.from(token.split(".")[0], "base64").toString());
};

export const sleep = (ms: number) => {
    return new Promise((resolve) => setTimeout(() => resolve(true), ms));
};

export const checkIsToday = (timestamp: number = 0) => {
    return moment().isSame(moment.unix(timestamp), "day");
};


export const formatAddress = (str: string) => {
    return str.replace(/^(.{4})(.{4})(.*)$/, "$1 $2 $3");
}