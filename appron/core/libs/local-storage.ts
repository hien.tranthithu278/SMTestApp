import { isEmpty } from "lodash";
import AsyncStorage from "@react-native-async-storage/async-storage";

const STORAGE_KEY = "LOCAL_STORAGE_KEY";

let storageBuffer: { [key: string]: any } = {};

export const setStorageItem = async (key: string, value: any) => {
  if (isEmpty(key)) {
    throw new Error("Invalid key");
  }

  storageBuffer[key] = value;
  await AsyncStorage.setItem(STORAGE_KEY, JSON.stringify(storageBuffer));
};

export const getStorageItem = (key: string) => {
  return storageBuffer[key];
};

export const initStorage = async () => {
  const item = await AsyncStorage.getItem(STORAGE_KEY);

  if (!!item && !isEmpty(item)) {
    storageBuffer = JSON.parse(item);
  }
};
