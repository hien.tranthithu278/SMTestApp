import { axios } from "../libs/custom-axios";
import { API_URL } from "../../config";

const CURRENCY_URL = `${API_URL}/currencies`;

export const CurrencyAPI = {
    get: () => {
        return axios.get(CURRENCY_URL);
    },
};
