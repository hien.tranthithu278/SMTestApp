import { axios } from "../libs/custom-axios";
import { API_URL } from "../../config";

const AUTH_URL = `${API_URL}/auth`;

export const AuthAPI = {
    login: (password: string) => {
        return axios.post(`${AUTH_URL}/login`, { password });
    },
};
