import { axios } from "../libs/custom-axios";
import { API_URL } from "../../config";

const ACCOUNT_URL = `${API_URL}/accounts`;

export const AccountAPI = {
    get: (userId: string) => {
        return axios.get(ACCOUNT_URL, { params: {userId} });
    },

    create: (data: {userId: string, name: string}) => {
        return axios.post(ACCOUNT_URL, data);
    },

    update: (accountId: string, name: string) => {
        return axios.post(`${ACCOUNT_URL}/${accountId}`, {name});
    },
};
