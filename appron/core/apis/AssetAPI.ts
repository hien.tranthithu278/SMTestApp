import { axios } from "../libs/custom-axios";
import { API_URL } from "../../config";

const ASSET_URL = `${API_URL}/assets`;

export const AssetAPI = {
    get: (accountId: string) => {
        return axios.get(ASSET_URL, { params: {accountId} });
    },

    transfer: (data: {from: string, to: string, symbol: string, amount: number}) => {
        return axios.patch(`${ASSET_URL}/transfer`, data);
    },
};
