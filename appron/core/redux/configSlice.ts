import { createSlice, PayloadAction } from "@reduxjs/toolkit";

import { getStorageItem, setStorageItem } from "../libs/local-storage";

import { AppThunk } from ".";
interface IConfigState {
    isShowLoading: boolean;
    currentTheme: "dark" | "light";
    locale: "vi" | "en";
}

const initialState: IConfigState = {
    isShowLoading: false,
    currentTheme: "dark",
    locale: "vi",
};

const config = createSlice({
    name: "config",
    initialState,
    reducers: {
        setIsLoading(state: IConfigState, action: PayloadAction<boolean>) {
            state.isShowLoading = action.payload;
        },
        setLocale(state: IConfigState, action: PayloadAction<"vi" | "en">) {
            state.locale = action.payload;
            setStorageItem("locale", action.payload);
        },
    },
});

export const { setIsLoading, setLocale } = config.actions;
export const configReducer = config.reducer;

const showLoad = (): AppThunk => async (dispatch) => {
    dispatch(setIsLoading(true));
};

const hideLoad = (): AppThunk => async (dispatch) => {
    dispatch(setIsLoading(false));
};

const load = (): AppThunk => async (dispatch) => {
    let locale = getStorageItem("locale");
    dispatch(setLocale(["vi", "en"].includes(locale) ? locale : "vi"));
};

const enableLoad =
    (enabled: boolean): AppThunk =>
    async (dispatch) => {
        dispatch(setIsLoading(enabled));
    };

export const configActions = {
    showLoad,
    hideLoad,
    enableLoad,
    load,
};
