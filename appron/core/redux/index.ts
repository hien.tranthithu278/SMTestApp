import { Action, configureStore } from "@reduxjs/toolkit";
import { ThunkAction } from "redux-thunk";

import rootReducer, { AppState } from "./rootReducer";

const store = configureStore({
  reducer: rootReducer,
  devTools: __DEV__ === true,
});

export type AppDispatch = typeof store.dispatch;
export type AppThunk = ThunkAction<void, AppState, null, Action<string>>;
export default store;
