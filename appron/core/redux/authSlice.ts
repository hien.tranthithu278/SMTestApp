import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { get } from "lodash";

import { AppThunk } from ".";
import { AuthAPI } from "../apis/AuthAPI";
import {  extractResponseError, decodeToken } from "../libs/utils";
import { getStorageItem, setStorageItem } from "../libs/local-storage";

import { configActions } from "./configSlice";
import { API_URL } from "../../config";
import { dataActions } from "./dataSlice";

interface IAuthState {
    isLoggedIn: boolean;
    authInitState: "waiting" | "init" | "done" | "fail" | "noauth";
    error?: string | null;
}

const initialState: IAuthState = {
    isLoggedIn: false,
    authInitState: "waiting",
    error: null,
};

const auth = createSlice({
    name: "auth",
    initialState,
    reducers: {
        authBegin(state: IAuthState) {
            state.error = null;
        },
        authSuccess(state: IAuthState) {
            state.isLoggedIn = true;
            state.authInitState = "done";
        },
        authFailure(state: IAuthState, action: PayloadAction<string>) {
            state.isLoggedIn = false;
            state.authInitState = "fail";
            state.error = action.payload;
        },
        authInit(state: IAuthState) {
            state.isLoggedIn = false;
            state.authInitState = "noauth";
        },
    },
});

export const { authBegin, authSuccess, authFailure, authInit } = auth.actions;
export const authReducer = auth.reducer;

const login =
    (password: string): AppThunk =>
    async (dispatch, getState) => {
        try {
            console.log("login - try");
            dispatch(authInit())
            dispatch(configActions.showLoad());
            const {data} = await AuthAPI.login(password);

            const { userId } = decodeToken(data.accessToken);
            await dispatch(dataActions.getAccounts(userId));
            dispatch(authSuccess());
            dispatch(configActions.hideLoad());
        } catch (err) {
            console.log("login - catch");
            dispatch(authFailure(extractResponseError(err)));
        } finally {
            dispatch(configActions.hideLoad());
        }
    };


const logout =
    (callback?: VoidFunction): AppThunk =>
    async (dispatch: any) => {
        console.log("logout");
        // const token = (await Notifications.getExpoPushTokenAsync()).data;
        // await deletePushNotificationToken(token);
        const emptyAuth = { accessToken: "", refreshToken: "" };
        // dispatch(getProfileFailure(""));
        dispatch(authInit());
        await setStorageItem("auth", emptyAuth);
        !!callback && callback();
    };

export const authActions = {
    login,
    logout,
};
