import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import {find, get} from "lodash";
import { AccountAPI } from "../apis/AccountAPI";
import { AssetAPI } from "../apis/AssetAPI";
import {CurrencyAPI} from "../apis/CurrencyAPI"
import { extractResponseError } from "../libs/utils";
import { IAccount, IAsset, IExchangeRate } from "../../interface";

import { AppThunk } from ".";

interface IMeState {
    error: string | null;
    accounts: IAccount[] | null;
    currentAccount: IAccount;
    currentAssets: IAsset[] | null;
    userId: string;
    rate: {
        EURVND: number;
        EURUSD: number;
    };
    totalEUR: number;
}

const initialState: IMeState = {
    userId: "",
    accounts: [],
    currentAccount: {
        id: "",
        address: "",
        name: ""
    },
    currentAssets: [],
    error: null,
    totalEUR: 0,
    rate: {
        EURVND: 0,
        EURUSD: 0
    }
};

const data = createSlice({
    name: "data",
    initialState,
    reducers: {
        getDataBegin(state: IMeState) {
            state.error = null;
        },
        getAccountsSuccess(
            state: IMeState,
            action: PayloadAction<{
                accounts: IAccount[];
                userId: string;
            }>
        ) {
            const { accounts, userId} = action.payload;
            state.accounts = accounts;
            state.currentAccount = accounts[0];
            state.userId = userId;
        },
        setCurrentAccount(state: IMeState,
            action: PayloadAction<{
                account: IAccount;
            }>
        ){
            const { account } = action.payload;
            state.currentAccount = account;
        },
        getAssetsSuccess(state: IMeState,
            action: PayloadAction<{
                assets: IAsset[];
            }>
        ){
            const { assets } = action.payload;
            state.currentAssets = assets;
            
            let total = 0;
            assets.forEach(item => {
                total += item.balance * 1 / get(item, "currency.rate", 0)
            });
            state.totalEUR = total;
        },
        getRatesSuccess(state: IMeState,
            action: PayloadAction<{
                rate: {
                    EURVND: number;
                    EURUSD: number;
                }
            }>
        ){
            const { rate } = action.payload;
            state.rate = rate;
        },
        setError(state: IMeState, { payload }: PayloadAction<string>) {
            state.error = payload;
        },
        clearError(state: IMeState) {
            state.error = null;
        },
    },
});

export const {
    getDataBegin,
    getAccountsSuccess,
    setCurrentAccount,
    getAssetsSuccess,
    setError,
    clearError,
    getRatesSuccess,
} = data.actions;
export const dataReducer = data.reducer;

const getAccounts =
    (userId: string): AppThunk =>
    async (dispatch: any) => {
        try {
            dispatch(getDataBegin());
            const res = await AccountAPI.get(userId);
            const accounts = res.data;
            await dispatch(getAccountsSuccess({accounts, userId}));
        } catch (err) {
            // console.log(err);
            dispatch(setError(extractResponseError(err)));
        }
    };

const getAssets = (): AppThunk =>
    async (dispatch: any, getState: any) => {
        try {
            dispatch(getDataBegin());
            const {id} = getState().data.currentAccount;
            const res = await AssetAPI.get(id);
            const assets = res.data;
            console.log(res)
            dispatch(getAssetsSuccess({assets}));
        } catch (err) {
            dispatch(setError(extractResponseError(err)));
        }
    };

const getRates = (): AppThunk =>
    async (dispatch: any, getState: any) => {
        try {
            dispatch(getDataBegin());
            const res = await CurrencyAPI.get();
            const currencies = res.data;
            console.log(currencies);
            const { rate: EURVND } = find(currencies, {base: "EUR", quote: "VND"})
            const { rate: EURUSD } = find(currencies, {base: "EUR", quote: "USD"})
            dispatch(getRatesSuccess({rate : {
                EURVND,
                EURUSD
            }}));
        } catch (err) {
            dispatch(setError(extractResponseError(err)));
        }
    };

export const dataActions = {
    getAccounts,
    getAssets,
    getRates,
    setCurrentAccount,
    clearError,
};
