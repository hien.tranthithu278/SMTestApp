import { combineReducers } from "@reduxjs/toolkit";

import { authReducer } from "./authSlice";
import { dataReducer } from "./dataSlice";
import { configReducer } from "./configSlice";

const rootReducer = combineReducers({
  auth: authReducer,
  data: dataReducer,
  config: configReducer,
});

export type AppState = ReturnType<typeof rootReducer>;

export default rootReducer;
