export const LOGO = require("./images/logo.png");
export const LOGO_WHITE = require("./images/logo-white.png");
export const BG = require("./images/bg.png");
export const USER = require("./icons/user.png");
export const DEPOSIT = require("./icons/deposit.png");
export const SEND = require("./icons/send.png");
export const SWAP = require("./icons/swap.png");

export const CURRENCIES = {
    EUR: require("./currencies/eur.png"),
    JPY: require("./currencies/yen.png"),
    USD: require("./currencies/usd.png")
}