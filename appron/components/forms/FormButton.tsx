import React, { FC } from "react";
import { StyleProp, TextStyle, ViewStyle, Pressable, StyleSheet, View } from "react-native";
import { LinearGradient } from 'expo-linear-gradient';
import { Text } from "../Themed"

export interface IFormButtonProps {
    icon?: string;
    disabled?: boolean;
    mode?: "text" | "outlined" | "contained";
    onPress?: VoidFunction;
    content?: string;
    containerStyle?: StyleProp<ViewStyle>;
    textStyle?: StyleProp<TextStyle>;
    buttonColors?: string[]
}

const FormButton: FC<IFormButtonProps> = ({
    icon,
    disabled = false,
    onPress = () => { },
    mode = "contained",
    children,
    content = undefined,
    containerStyle = {},
    textStyle = {},
    buttonColors = ['#1C94F4', '#1273EA']
}) => {
    return (
        <Pressable
            disabled={disabled}
            onPress={onPress}
            style={[styles.defaultStyle, containerStyle]}
        >
            {buttonColors.length == 2 ?
                <LinearGradient
                    colors={buttonColors}
                    style={[styles.button, { opacity: disabled ? 0.5 : 1 }]}>
                    {content ? (
                        <Text style={[styles.content, textStyle]}>{content}</Text>
                    ) : (
                        children
                    )}
                </LinearGradient> :
                <View
                    style={[styles.button, { backgroundColor: buttonColors[0], opacity: disabled ? 0.5 : 1 }]}>
                    {content ? (
                        <Text style={[styles.content, textStyle]}>{content}</Text>
                    ) : (
                        children
                    )}
                </View>}
        </Pressable>
    );
};

export default FormButton;

const styles = StyleSheet.create({
    defaultStyle: {

    },
    button: {
        paddingHorizontal: 20,
        paddingVertical: 10,
        alignItems: "center",
        justifyContent: "center",
        borderRadius: 8,
        height: 45
    },
    content: {
        fontWeight: "600",
        color: "#fff",
        fontSize: 16
    }
})