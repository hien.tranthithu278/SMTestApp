import React, { FC, ReactNode } from "react";
import {
    View,
    TextInputProps,
    Pressable,
    StyleProp,
    ViewStyle,
    TextInput
} from "react-native";

import { Text } from "../Themed"

interface IProps extends TextInputProps {
    label?: string;
    value?: string;
    message?: string;

    disabled?: boolean;

    style?: StyleProp<ViewStyle>;
    secureTextEntry?: boolean;
    onChangeText: (text: string) => void;

    onPress?: VoidFunction;
    mode?: "outlined" | "flat" | undefined;
}

const FormInput: FC<IProps> = ({
    label = "",
    value = "",
    message = "",
    editable = true,
    secureTextEntry = false,
    style = {},
    onChangeText = (text: string) => { },
    onPress = () => { },
    mode = "outlined",
}) => {
    return (
        <View
            style={[{ width: "100%", padding: 20 }]}
        >
            {!!label && (
                <Text
                    style={{ color: "#57627B", marginBottom: 5, marginLeft: 15 }}
                >
                    {label}
                </Text>
            )}
            <TextInput
                value={value}
                onChangeText={(text) => onChangeText(text)}
                editable={editable}
                secureTextEntry={secureTextEntry}
                style={{
                    backgroundColor: editable ? "#fff" : "#EDF1F7",
                    color: editable ? "#151A30" : "#8F9BB3",
                    borderColor: "#C5CEE0",
                    borderRadius: 8,
                    width: "100%",
                    height: 40,
                    borderWidth: 1,
                    padding: 20
                }}
            />
            {!!message && (
                <Text
                    style={{ color: "red" }}
                >
                    {message}
                </Text>
            )}
            {/* {buttonLike && (
                <Pressable
                    onPress={onPress}
                    style={{
                        position: "absolute",
                        width: "100%",
                        height: "100%",
                        backgroundColor: "rgba(0, 0, 0, 0)",
                    }}
                />
            )} */}
        </View>
    );
};

export default FormInput;
