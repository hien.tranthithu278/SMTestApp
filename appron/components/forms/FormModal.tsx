import React, {
    forwardRef,
    ForwardRefRenderFunction,
    ReactNode,
    useImperativeHandle,
    useState,
} from 'react';
import { TouchableHighlight, Modal, View, Pressable, StyleProp, ViewStyle } from 'react-native';
import { MaterialIcons } from '@expo/vector-icons';

interface IProps {
    defaultVisible?: boolean;
    closeIcon?: boolean;
    containerStyle?: StyleProp<ViewStyle>;
    touchZoneStyle?: StyleProp<ViewStyle>;
    children: ReactNode;
}

const FormModal: ForwardRefRenderFunction<unknown, IProps> = (
    {
        defaultVisible = false,
        closeIcon = false,
        containerStyle = {},
        touchZoneStyle = {},
        children,
    },
    ref
) => {
    const [visible, setVisible] = useState(defaultVisible);

    useImperativeHandle(ref, () => ({
        show: () => {
            setVisible(true);
        },
        hide: () => {
            setVisible(false);
        },
        getVisible: () => {
            return visible;
        },
    }));

    return (
        <Modal transparent={true} visible={visible}>
            <Pressable
                style={[
                    {
                        flex: 1,
                        justifyContent: 'center',
                        alignItems: 'center',
                        backgroundColor: 'rgba(48, 48, 48, 0.5)',
                        overflow: 'hidden',
                    },
                    containerStyle,
                ]}
                onPress={() => setVisible(false)}
            >
                <View
                    style={[
                        {
                            width: '100%',
                            padding: 20,
                        },
                        touchZoneStyle,
                    ]}
                >
                    <TouchableHighlight style={{ width: '100%' }} underlayColor="#fff">
                        <>
                            {closeIcon && (
                                <MaterialIcons
                                    name="close"
                                    size={30}
                                    color="#333"
                                    style={{
                                        position: 'absolute',
                                        right: 8,
                                        top: 3,
                                        zIndex: 3,
                                    }}
                                    onPress={() => setVisible(false)}
                                />
                            )}
                            {children}
                        </>
                    </TouchableHighlight>
                </View>
            </Pressable>
        </Modal>
    );
};

export default forwardRef(FormModal);
