import React, { FC, ReactNode, useState } from "react";
import {
    View,
    TextInputProps,
    StyleProp,
    ViewStyle,
    TextInput,
    StyleSheet
} from "react-native";

import { Text } from "../Themed";
import Layout from "../../constants/Layout"

interface IProps extends TextInputProps {
    label?: string;
    value?: string;
    message?: string;

    disabled?: boolean;

    style?: StyleProp<ViewStyle>;
    secureTextEntry?: boolean;
    onChangeText?: (text: string) => void;

    onPress?: VoidFunction;
    mode?: "outlined" | "flat" | undefined;

    rightBtn?: ReactNode,
    labelRight?: string,
}

const FormInput: FC<IProps> = ({
    label = "",
    value,
    message = "",
    editable = true,
    secureTextEntry = false,
    style = {},
    onChangeText = (text: string) => { },
    onPress = () => { },
    mode = "outlined",
    rightBtn = null,
    labelRight = ""
}) => {
    const [isOnFocus, setIsOnFocus] = useState(false);

    return (
        <View
            style={[{ width: "100%", marginVertical: 10 }, style]}
        >
            <View style={{ flexDirection: "row", justifyContent: "space-between" }}>
                {!!label ? (
                    <Text
                        style={{ color: "#57627B", marginBottom: 5, marginLeft: 15, fontWeight: "bold" }}
                    >
                        {label}
                    </Text>) : <View />
                }
                {!!labelRight ? (
                    <Text
                        style={{ color: "#151A30", marginBottom: 5, marginLeft: 15, fontWeight: "bold" }}
                    >
                        {labelRight}
                    </Text>) : <View />
                }
            </View>
            <TextInput
                value={value}
                onChangeText={onChangeText}
                editable={editable}
                secureTextEntry={secureTextEntry}
                onFocus={() => setIsOnFocus(true)}
                onBlur={() => setIsOnFocus(false)}
                style={{
                    backgroundColor: editable ? "#fff" : "#EDF1F7",
                    color: editable ? "#151A30" : "#8F9BB3",
                    borderColor: isOnFocus ? "#1273EA" : "#C5CEE0",
                    borderRadius: 8,
                    width: "100%",
                    height: Layout.inputHeight,
                    borderWidth: 1,
                    paddingHorizontal: 20
                }}
            />
            {rightBtn &&
                <View style={styles.rightBtn}>
                    {rightBtn}
                </View>
            }

            {!!message && (
                <Text
                    style={{ color: "red", marginLeft: 15 }}
                >
                    {message}
                </Text>
            )}
        </View>
    );
};

export default FormInput;

const styles = StyleSheet.create({
    rightBtn: {
        position: "absolute",
        top: 20,
        right: 0,
        paddingHorizontal: 20,
        alignItems: "center",
        justifyContent: "center",
        height: Layout.inputHeight
    }
})