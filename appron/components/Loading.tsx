import React, { FC } from "react";
import { ActivityIndicator, View } from "react-native";
import { useSelector } from "react-redux";

import { AppState } from "../core/redux/rootReducer";

const Loading: FC = () => {
    const { isShowLoading } = useSelector((state: AppState) => state.config);

    if (!isShowLoading) return null;

    return (
        <View
            style={{
                position: "absolute",
                top: 0,
                left: 0,
                width: "100%",
                height: "100%",
                backgroundColor: "rgba(0,0,0,0.5)",
                alignItems: "center",
                justifyContent: "center",
            }}
        >
            <ActivityIndicator size="large" />
        </View>
    );
};

export default Loading;
