import React, { FC, useEffect, useState, useCallback } from "react";
import { useNavigation } from "@react-navigation/native";
import { useFocusEffect } from "@react-navigation/native";

import { Image, ImageBackground, Pressable, KeyboardAvoidingView, Platform, Alert, BackHandler } from "react-native";
import { Ionicons } from '@expo/vector-icons';
import { Text } from "../components/Themed";
import { FormInput, FormButton } from "../components/forms";

import Layout from "../constants/Layout";
import { LOGO, BG } from "../assets/index";

import { useDispatch, useSelector } from "react-redux";
import { authActions } from "../core/redux/authSlice";
import { AppState } from "../core/redux/rootReducer";

interface IProps { }

const LoginScreen: FC<IProps> = ({ }) => {
  const navigation = useNavigation();
  const [password, setPassword] = useState("");
  const [isShowPassword, setIsShowPassword] = useState(false);
  const dispatch = useDispatch();
  const { isLoggedIn, authInitState, error } = useSelector(
    (state: AppState) => state.auth,
  );

  useFocusEffect(
    useCallback(() => {
      const onBackPress = () => {
        Alert.alert(
          "Hold on!",
          "Are you sure you want to go back?",
          [
            {
              text: "Cancel",
              onPress: () => null,
              style: "cancel",
            },
            {
              text: "Ok",
              onPress: () => BackHandler.exitApp(),
            },
          ],
          { cancelable: false },
        );
        return true;
      };

      BackHandler.addEventListener("hardwareBackPress", onBackPress);

      return () =>
        BackHandler.removeEventListener(
          "hardwareBackPress",
          onBackPress,
        );
    }, []),
  );

  useEffect(() => {
    if (authInitState === "done") {
      navigation.navigate("MainStack");
    }
  }, [isLoggedIn, authInitState]);

  return (
    <KeyboardAvoidingView style={{ flex: 1 }}
      behavior={
        Platform.OS === "ios" ? "padding" : "height"
      }>
      <ImageBackground
        source={BG}
        style={{ flex: 1, justifyContent: "center", alignItems: "center", padding: Layout.mainPadding }}
        resizeMode="cover"
      >
        <Image
          source={LOGO}
          style={{
            width: 180,
            height: 230,
            resizeMode: "contain"
          }}
        />
        <Text style={{ fontSize: 40, fontWeight: "bold" }}>
          Ronin Wallet
        </Text>
        <Text style={{ fontSize: 16, marginTop: 10, marginBottom: 20 }}>
          Your Digital Passport
        </Text>
        <FormInput
          label="ENTER PASSWORD"
          value={password}
          onChangeText={setPassword}
          secureTextEntry={!isShowPassword}
          rightBtn={
            <Pressable
              onPress={() => setIsShowPassword(!isShowPassword)}
            >
              <Ionicons
                name={isShowPassword ? "ios-eye-outline" : "ios-eye-off-outline"}
                size={24}
                color="#231F20"
              />
            </Pressable>
          }
          message={error ? error : ""}
        />

        <FormButton
          content="Unlock"
          onPress={() => {
            dispatch(authActions.login(password));
          }}
          containerStyle={{ marginTop: 20 }}
          disabled={password === ""}
        />
      </ImageBackground >
    </KeyboardAvoidingView>
  );
};

export default LoginScreen;
