import React, { FC, useEffect } from "react";
import { useNavigation } from "@react-navigation/native";
import { Image, ImageBackground } from "react-native";

import { LOGO, BG } from "../assets/index";

interface IProps { }

const LoadingScreen: FC<IProps> = ({ }) => {
  const navigation = useNavigation();

  useEffect(() => {
    navigation.navigate("LoginScreen")
  }, []);

  return (
    <ImageBackground
      source={BG}
      style={{ flex: 1, justifyContent: "center", alignItems: "center" }}
      resizeMode="cover"
    >
      <Image
        source={LOGO}
        style={{
          width: 180,
          resizeMode: "contain"
        }}
      />
    </ImageBackground >
  );
};

export default LoadingScreen;
