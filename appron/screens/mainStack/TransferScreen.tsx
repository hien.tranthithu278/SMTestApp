import React, { FC, useState, useRef, useEffect } from 'react';
import { get, find } from "lodash";
import { useDispatch, useSelector } from "react-redux";

import { MainStackProps } from "../../types";
import Layout from '../../constants/Layout';
import { formatAddress } from "../../core/libs/utils";

import { Pressable, StyleSheet, View, KeyboardAvoidingView, Platform, ScrollView, Keyboard } from 'react-native';
import { Text } from '../../components/Themed';
import { FormInput, FormButton, FormModal } from "../../components/forms";
import SelectAsset from "./components/SelectAsset";

import { ICurrency } from '../../interface';
import { dataActions } from "../../core/redux/dataSlice";
import { AppState } from "../../core/redux/rootReducer";
import { AssetAPI } from "../../core/apis/AssetAPI"

export default function TransferScreen(
  { navigation, route }: MainStackProps<"TransferScreen">,
) {
  const modalRef = useRef<any>(null);
  const dispatch = useDispatch();
  const { currentAssets, currentAccount } = useSelector(
    (state: AppState) => state.data,
  );
  const [selectedAsset, setSelectedAsset]: [ICurrency | null, any] = useState(get(route, "params.currency", null))

  const [amount, setAmount]: [string, any] = useState("");
  const [to, setTo]: [string, any] = useState("");
  const [error, setError]: [string, any] = useState("");
  const maxAmount = get(find(currentAssets, { symbol: selectedAsset }), "balance", 0);

  const [isKeyboardVisible, setKeyboardVisible] = useState(false);

  useEffect(() => {
    const keyboardDidShowListener = Keyboard.addListener(
      'keyboardDidShow',
      () => {
        setKeyboardVisible(true); // or some other action
      }
    );
    const keyboardDidHideListener = Keyboard.addListener(
      'keyboardDidHide',
      () => {
        setKeyboardVisible(false); // or some other action
      }
    );

    return () => {
      keyboardDidHideListener.remove();
      keyboardDidShowListener.remove();
    };
  }, []);

  return (
    <View style={{ flex: 1 }}>
      <View style={styles.container}>
        <KeyboardAvoidingView style={{ flex: 1 }}
          behavior={
            Platform.OS === "ios" ? "padding" : undefined
          }
        >
          <ScrollView style={styles.main}>
            <FormInput
              label='FROM'
              value={`My Wallet (${formatAddress(currentAccount.address)})`}
              editable={false}
            />
            <FormInput
              label='TO'
              value={to}
              onChangeText={setTo}
            />
            <SelectAsset
              value={selectedAsset}
              onChange={(value) => setSelectedAsset(value)}
            />
            <FormInput
              label='AMOUNT'
              value={amount}
              onChangeText={setAmount}
              labelRight={selectedAsset ? `AVAILABLE: ${maxAmount} ${selectedAsset}` : ""}
              rightBtn={(
                <Pressable style={styles.btnRightTouchZone} onPress={() => setAmount(maxAmount.toString())}>
                  <View style={styles.btnRight}>
                    <Text style={{ color: "#57627B", fontSize: 10 }}>MAX</Text>
                  </View>
                </Pressable>
              )}
              message={error}
            />
          </ScrollView>
        </KeyboardAvoidingView>
        {!isKeyboardVisible && <View style={styles.footer}>
          <FormButton
            content="Cancel"
            containerStyle={{ margin: 10, flex: 1 }}
            buttonColors={["#F7F9FC"]}
            textStyle={{ color: "#1273EA" }}
            onPress={() => navigation.goBack()}
          />
          <FormButton
            disabled={to === "" || amount === ""}
            content="Send"
            containerStyle={{ margin: 10, flex: 1 }}
            onPress={() => {
              setError("");
              AssetAPI.transfer({
                from: currentAccount.address,
                to,
                symbol: selectedAsset as string,
                amount: parseFloat(amount)
              }).then(res => {
                dispatch(dataActions.getAssets())
                modalRef.current.show()
              }).catch(err => {
                setError(get(err, "response.data.message", "Error!"))
              });
            }}
          />
        </View>}
      </View>
      <FormModal
        ref={modalRef}
      >
        <View style={styles.modalContainer}>
          <Text style={styles.modalTitle}>Successfully sent</Text>
          <View>
            <Text>Your <Text style={styles.bold}>{selectedAsset}</Text> has been send!</Text>
            <Text>Thank you for using our service</Text>
          </View>
          <FormButton
            content="OK"
            containerStyle={{ marginTop: 20 }}
            onPress={() => {
              modalRef.current.hide();
              navigation.goBack()
            }}
          />
        </View>
      </FormModal >
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    justifyContent: "space-between"
  },
  main: {
    flex: 1,
    paddingHorizontal: Layout.mainPadding,
  },
  footer: {
    flexDirection: "row",
    padding: 10,
    paddingBottom: 15,
    backgroundColor: "#fff"
  },
  modalContainer: {
    backgroundColor: "#fff",
    borderRadius: 16,
    padding: 20,
    height: 205,
    justifyContent: "space-between",
    flexDirection: "column"
  },
  modalTitle: {
    textAlign: "center",
    fontWeight: "bold",
    fontSize: 22,
    marginBottom: 20
  },
  bold: {
    fontWeight: "bold"
  },
  btnRightTouchZone: {
    height: "100%",
    width: 70,
    marginTop: Platform.OS === "ios" ? 4 : 14,
    justifyContent: "center",
    paddingHorizontal: 5,
    marginRight: -10
  },
  btnRight: {
    backgroundColor: "#EEF3FB",
    paddingHorizontal: 15,
    paddingVertical: 5,
    borderRadius: 10,
    alignItems: "center"
  }
});
