import { useEffect, useCallback } from "react";
import { useFocusEffect } from "@react-navigation/native";
import { get } from "lodash";

import { MainStackProps, MainStackParamList } from "../../types";
import { USER, BG, DEPOSIT, SEND, SWAP, CURRENCIES } from "../../assets/index";
import { formatWithCommas } from "../../core/libs/utils"

import { StyleSheet, ImageBackground, SafeAreaView, Image, Pressable, ScrollView, View, Alert, BackHandler } from 'react-native';
import { Text } from '../../components/Themed';
import Information from "./components/Information";

import { useDispatch, useSelector } from "react-redux";
import { dataActions } from "../../core/redux/dataSlice";
import { AppState } from "../../core/redux/rootReducer";

export default function HomeScreen(
  { navigation }: MainStackProps<"HomeScreen">
) {

  const dispatch = useDispatch();
  const { currentAssets, rate, totalEUR, currentAccount } = useSelector(
    (state: AppState) => state.data,
  );

  useEffect(() => {
    dispatch(dataActions.getAssets());
    dispatch(dataActions.getRates())
  }, []);

  useFocusEffect(
    useCallback(() => {
      const onBackPress = () => {
        Alert.alert(
          "Hold on!",
          "Are you sure you want to go back?",
          [
            {
              text: "Cancel",
              onPress: () => null,
              style: "cancel",
            },
            {
              text: "Ok",
              onPress: () => BackHandler.exitApp(),
            },
          ],
          { cancelable: false },
        );
        return true;
      };

      BackHandler.addEventListener("hardwareBackPress", onBackPress);

      return () =>
        BackHandler.removeEventListener(
          "hardwareBackPress",
          onBackPress,
        );
    }, []),
  );

  return (
    <ImageBackground
      source={BG}
      style={styles.container}
      resizeMode="cover"
    >
      <SafeAreaView />
      <View style={styles.header}>
        <View style={styles.section}>
          <View style={styles.dot}></View>
          <Text>Ronin Wallet</Text>
        </View>
        <Pressable style={styles.section} onPress={() => navigation.navigate("LoginScreen")}>
          <Image source={USER} style={{ width: 25, height: 25, resizeMode: "contain" }} />
        </Pressable>
      </View>
      <ScrollView style={{ flex: 1, paddingHorizontal: 20 }}>
        <Information data={{
          address: currentAccount.address,
          totalVND: formatWithCommas(Math.round(totalEUR * rate.EURVND)),
          totalUSD: formatWithCommas(Math.round(totalEUR * rate.EURUSD))
        }}
        />
        <View style={styles.actions}>
          <View style={styles.actionItem}>
            <View style={[styles.section, { opacity: 0.5 }]}>
              <Image source={DEPOSIT} style={styles.imgAction} />
            </View>
            <Text style={styles.titleAction}>Deposit</Text>
          </View>
          <View style={styles.actionItem}>
            <Pressable style={styles.section} onPress={() => navigation.navigate("TransferScreen", { currency: null })}>
              <Image source={SEND} style={styles.imgAction} />
            </Pressable>
            <Text style={styles.titleAction}>Send</Text>
          </View>
          <View style={styles.actionItem}>
            <View style={[styles.section, { opacity: 0.5 }]}>
              <Image source={SWAP} style={styles.imgAction} />
            </View>
            <Text style={styles.titleAction}>Swap</Text>
          </View>
        </View>
        <View style={styles.assets}>
          <Text style={{ fontWeight: "bold", fontSize: 16, marginBottom: 10 }}>Assets</Text>
          {currentAssets && currentAssets.map((asset) => {
            console.log(asset)
            return (
              <Pressable style={styles.asset} key={asset.id} onPress={() => navigation.navigate("TransferScreen", { currency: asset.symbol })}>
                <Image source={CURRENCIES[asset.symbol]} style={styles.imgAction} />
                <View style={{ marginLeft: 10 }}>
                  <Text style={{ fontWeight: "bold", fontSize: 16 }}>{asset.balance} {asset.symbol}</Text>
                  <Text>{formatWithCommas(Math.round(1 / get(asset, "currency.rate", 0) * rate.EURVND * asset.balance))} VND</Text>
                </View>
              </Pressable>
            )
          })}
        </View>
      </ScrollView>
    </ImageBackground>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  header: {
    flexDirection: "row",
    justifyContent: "space-between",
    padding: 20,
    paddingBottom: 10
  },
  section: {
    backgroundColor: "#F7F9FC",
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    padding: 15,
    borderRadius: 10
  },
  dot: {
    backgroundColor: "#1273EA",
    width: 8,
    height: 8,
    borderRadius: 4,
    marginRight: 8
  },
  actions: {
    width: "100%",
    alignItems: "center",
    marginVertical: 20,
    flexDirection: "row",
    justifyContent: "center"
  },
  actionItem: {
    alignItems: "center",
    margin: 10
  },
  assets: {
  },
  imgAction: {
    width: 35, height: 35, resizeMode: "contain"
  },
  titleAction: { fontSize: 14, fontWeight: "600", marginTop: 10 },
  asset: {
    backgroundColor: "#F7F9FC",
    paddingHorizontal: 20,
    paddingVertical: 10,
    borderRadius: 8,
    marginVertical: 5,
    flexDirection: "row",
    alignItems: "center"
  }
});
