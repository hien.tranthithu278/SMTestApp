import React, { FC, useState, useRef } from 'react';
import { View, Image, StyleProp, ViewStyle, Pressable, StyleSheet, TouchableHighlight } from 'react-native';
import { find, get } from 'lodash';

import Layout from "../../../constants/Layout";

import { FormModal } from '../../../components/forms';
import { AntDesign, Ionicons } from '@expo/vector-icons';
import { Text } from "../../../components/Themed";

import { ICurrency } from "../../../interface";
import { CURRENCIES } from "../../../assets/index";
import { formatWithCommas } from "../../../core/libs/utils"

import { useDispatch, useSelector } from "react-redux";
import { AppState } from "../../../core/redux/rootReducer";

interface IProps {
    label?: string;
    value: ICurrency | null;
    style?: StyleProp<ViewStyle>;
    options?: { label: string; value: any }[];
    onChange?: (value: any) => void;
    mode?: 'outlined' | 'flat' | undefined;
}

const FormSelect: FC<IProps> = ({
    value,
    options = [],
    onChange = (value) => { },
}) => {
    const modalRef = useRef<any>(null);

    const dispatch = useDispatch();
    const { currentAssets, rate } = useSelector(
        (state: AppState) => state.data,
    );

    return (
        <View style={{ marginVertical: 10 }}>
            <View >
                <Text style={styles.label}>ASSETS</Text>
                <Pressable
                    onPress={() => modalRef.current.show()}
                    style={styles.input}
                >
                    {value !== null ?
                        <View style={{ flexDirection: "row", alignItems: "center" }}>
                            <Image source={CURRENCIES[value]} style={styles.img} />
                            <Text style={{ textTransform: "uppercase", fontSize: 16, margin: 5 }}>{value}</Text>
                        </View>
                        : <View />
                    }
                    <Ionicons name="layers-outline" size={24} color="#231F20" />
                </Pressable>
            </View>
            <FormModal
                ref={modalRef}
            >
                <View
                    style={{
                        backgroundColor: "#fff",
                        borderRadius: 8
                    }}
                >
                    <View style={styles.headerModal}>
                        <Text style={styles.labelHeader}>Assets</Text>
                        <TouchableHighlight
                            style={styles.closeBtn}
                            onPress={() => modalRef.current.hide()}
                            underlayColor="#EDF1F7"
                            activeOpacity={1}
                        >
                            <AntDesign name="close" size={24} color="#57627B" />
                        </TouchableHighlight>
                    </View>
                    <View style={styles.assets}>
                        {currentAssets && currentAssets.map(asset => {
                            return (
                                <TouchableHighlight
                                    underlayColor="#EDF1F7"
                                    activeOpacity={1}
                                    onPress={() => {
                                        onChange(asset.symbol);
                                        modalRef.current.hide()
                                    }}
                                    key={asset.id}
                                >
                                    <View style={styles.asset}>
                                        <Image source={CURRENCIES[asset.symbol]} style={styles.img} />
                                        <View style={{ marginLeft: 10 }}>
                                            <Text style={{ fontWeight: "bold", fontSize: 14 }}>{formatWithCommas(asset.balance)} {asset.symbol}</Text>
                                            <Text >{formatWithCommas(Math.round(1 / get(asset, "currency.rate", 0) * rate.EURVND * asset.balance))} VND</Text>
                                        </View>
                                    </View>
                                </TouchableHighlight>)
                        })}
                    </View>
                </View>
            </FormModal >
        </View >
    );
};

export default FormSelect;


const styles = StyleSheet.create({
    label: { color: "#57627B", marginBottom: 5, marginLeft: 15, fontWeight: "bold" },
    input: {
        backgroundColor: "#fff",
        color: "#151A30",
        borderColor: "#C5CEE0",
        borderRadius: 10,
        width: "100%",
        height: Layout.inputHeight,
        borderWidth: 1,
        paddingHorizontal: 20,
        justifyContent: "space-between",
        alignItems: "center",
        flexDirection: "row"
    },
    headerModal: {
        padding: 20,
        borderBottomWidth: 1,
        borderBottomColor: "#C5CEE0",
        justifyContent: "center",
        alignItems: "center"
    },
    labelHeader: {
        fontWeight: "600",
        fontSize: 18
    },
    assets: {
    },
    asset: {
        padding: 20,
        alignItems: "center",
        flexDirection: "row"
    },
    closeBtn: {
        position: "absolute",
        right: 0,
        top: 0,
        padding: 18,
        borderRadius: 10
    },
    img: {
        width: 30,
        height: 30,
        resizeMode: "contain"
    }
})