import React, { FC } from "react";
import { StyleSheet, View, Image, Pressable } from 'react-native';
import { Text } from '../../../components/Themed';
import { LinearGradient } from 'expo-linear-gradient';
import * as Clipboard from 'expo-clipboard';
import { formatAddress } from "../../../core/libs/utils";
import Toast from 'react-native-root-toast';

import { MainStackProps, MainStackParamList } from "../../../types";

import { Feather } from '@expo/vector-icons';

import { LOGO_WHITE, BG } from "../../../assets/index";

const Information: FC<{
    data: {
        address: string,
        totalVND: string,
        totalUSD: string
    }
}> = ({ data: { address, totalUSD, totalVND } }) => {
    return (
        <View style={styles.information}>
            <LinearGradient
                colors={['#1C94F4', '#1273EA']}
                style={styles.infoBG}
            >
                <View style={{
                    flexDirection: "row",
                    borderBottomWidth: 1,
                    borderBottomColor: "#68B8F8",
                    paddingBottom: 15,
                    marginBottom: 10,
                    justifyContent: "space-between",
                    flex: 0
                }}>
                    <Text>
                        <Text style={{ color: "#fff", fontSize: 16, fontWeight: "600" }}>My Wallet</Text>
                        <Text style={{ color: "#8DC9F9", fontSize: 14 }}>  ({formatAddress(address)})</Text>
                    </Text>
                    <Pressable onPress={() => {
                        Clipboard.setString(address);
                        Toast.show('Copied!', {
                            duration: Toast.durations.LONG,
                        });
                    }}>
                        <Feather name="copy" size={20} color="#fff" />
                    </Pressable>
                </View>
                <View style={styles.balanceContainer}>
                    <View style={{ marginBottom: 10 }}>
                        <Text style={{ color: "#fff", fontWeight: "bold", fontSize: 32, marginBottom: 10 }}>{totalUSD} USD</Text>
                        <Text style={{ color: "#8DC9F9", fontWeight: "600", fontSize: 18 }}>{totalVND} VND</Text>
                    </View>
                    <Image source={LOGO_WHITE} style={styles.logo} />
                </View>
            </LinearGradient >
        </View >
    );
}

export default Information;

const styles = StyleSheet.create({
    information: {
    },
    infoBG: {
        borderRadius: 16,
        width: "100%",
        height: "auto",
        padding: 20
    },
    actions: {},
    assets: {},
    logo: {
        alignSelf: "flex-end",
        width: 50,
        height: 50,
        resizeMode: "contain",
    },
    balanceContainer: {
        width: "100%",
        flexDirection: "row",
        justifyContent: "space-between"
    }
});
